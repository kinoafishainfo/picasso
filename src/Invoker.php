<?php

namespace Kinoafisha\Picasso;

use Kinoafisha\Picasso\Contracts\Command;

class Invoker
{
    /**
     * @var Command
     */
    protected $command;

    /**
     * @param Command $command
     */
    public function setCommand(Command $command)
    {
        $this->command = $command;
    }

    /**
     * Выполняет сохраненную команду
     */
    public function run()
    {
        $this->command->execute();
    }
}
