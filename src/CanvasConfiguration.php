<?php

namespace Kinoafisha\Picasso;

class CanvasConfiguration
{
    /**
     * Путь до файла подложки для текста поверх превью
     * При генерации превью с текстом сначала идет картинка превью, потом этот файл, затем текст
     *
     * @var string
     */
    protected $backgroundPath;

    /**
     * Путь до бэкграунда, если картинка для превью не найдена.
     * Мы можем генерировать превью даже если нет картинки
     *
     * @var string
     */
    protected $emptyBackgroundPath;

    /**
     * Путь до файла шрифта, которым мы будем писать подписи поверх превью
     *
     * @var string
     */
    protected $fontPath;

    /**
     * Какой драйвер мы будем использовать для рисования (gd, imagick)
     *
     * @var string
     */
    protected $imageDriver;

    /**
     * Дополнительная конфигурация для размещения текста
     * @var bool|array
     */
    protected $textArea;
    /**
     * CanvasConfiguration constructor.
     *
     * @param string $backgroundPath
     * @param string $emptyBackgroundPath
     * @param string $fontPath
     * @param string $imageDriver
     * @param array  $textArea
     */
    public function __construct($backgroundPath, $emptyBackgroundPath, $fontPath, $imageDriver = 'gd', $textArea = false)
    {
        $this->backgroundPath      = $backgroundPath;
        $this->emptyBackgroundPath = $emptyBackgroundPath;
        $this->fontPath            = $fontPath;
        $this->imageDriver         = $imageDriver;
        $this->textArea            = $textArea;
    }

    /**
     * @return string
     */
    public function getBackgroundPath()
    {
        return $this->backgroundPath;
    }

    /**
     * @return string
     */
    public function getEmptyBackgroundPath()
    {
        return $this->emptyBackgroundPath;
    }

    /**
     * @return string
     */
    public function getFontPath()
    {
        return $this->fontPath;
    }

    /**
     * @return string
     */
    public function getImageDriver()
    {
        return $this->imageDriver;
    }
    
    /**
     * @return string
     */
    public function getTextArea()
    {
    	return $this->textArea;
    }
}
