<?php

namespace Kinoafisha\Picasso\Commands;

use Intervention\Image\Image;

abstract class Announce extends Canvas
{
    /**
     * Возвращает фон для превью без картинки
     *
     * @param string|int $size   Желаемый размер (200x200, 580)
     * @param bool       $canvas Можно ли вырезать отдельные части. {@see resize()}
     * @return Image
     */
    protected function makeAnnounceBackground($size, $canvas)
    {
        $image = $this->imageManager->make($this->config->getEmptyBackgroundPath());

        $this->resizeImage($image, $size, $canvas);

        return $image;
    }
}
