<?php

namespace Kinoafisha\Picasso\Commands;

use Kinoafisha\Picasso\Contracts\Command;
use function Kinoafisha\Picasso\mb_wordwrap;

class MakeTrailerAnnounce extends Announce implements Command
{
    /**
     * Название фильма, разбитое по строкам
     *
     * @var array
     */
    protected $movieNameLines;

    /**
     * MakeTrailerAnnounce constructor.
     *
     * @param string $movieName
     * @param array  $params
     */
    public function __construct($movieName, ...$params)
    {
        parent::__construct(...$params);

        $this->movieNameLines = $this->splitMultilineText(mb_wordwrap($movieName, static::MAX_LINE_LENGTH));
    }

    /**
     * Делает превью для трейлера без картинки
     */
    public function execute()
    {
        $image = $this->makeAnnounceBackground($this->size, $this->canvas);

        $movieNameFontSize   = intval($image->width() * 54 / 1300); // Линейная зависимость
        $movieNameLineHeight = intval($movieNameFontSize * 60 / 54); // Линейная зависимость
        $titleFontSize       = intval($image->width() * 36 / 1300); // Линейная зависимость
        $titleLineHeight     = intval($titleFontSize * 42 / 36); // Линейная зависимость
        // Отступ между названием фильма и заголовком
        $paragraphMargin = intval($movieNameLineHeight * 0.8);

        // Высота текста заголовка
        $titleTextHeight = $titleFontSize * 0.6 + (count($this->titleLines) - 1) * $titleLineHeight;
        // Полная высота текста
        $totalTextHeight = $titleTextHeight + $paragraphMargin +
            $movieNameFontSize * 0.6 + (count($this->movieNameLines) - 1) * $movieNameLineHeight;

        // Откуда мы будем начинать писать заголовок
        $titleStartY = intval($image->height() / 2 + $totalTextHeight / 2);

        $this->addMultilineTextToImage(
            $image,
            $this->titleLines,
            $titleFontSize,
            $titleLineHeight,
            $titleStartY
        );

        $this->addMultilineTextToImage(
            $image,
            $this->movieNameLines,
            $movieNameFontSize,
            $movieNameLineHeight,
            intval($titleStartY - $titleTextHeight - $paragraphMargin)
        );

        $image->save($this->destPath, static::QUALITY);
    }
}
