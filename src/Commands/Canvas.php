<?php

namespace Kinoafisha\Picasso\Commands;

use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use Kinoafisha\Picasso\CanvasConfiguration;
use function Kinoafisha\Picasso\mb_wordwrap;

abstract class Canvas
{
    /**
     * Whitelist размеров картинок для манипуляций
     */
    const FORMATS = [
        '100x100',
        '450x300',
        '450x300!',
        '93x61',
        '300x200',
        '90x60',
        '150x100',
        '90x150',
        '270x',
        'x100',
        '600x400',
        'x220', // Кадры ios app
        '1200x900', // Кадры ios app (maximum)
        '1024x768', // Кадры новое кино
        '1024x768', // Кадры новое кино
        '1920x1080',  // Кадры тест
        '645x345!',  // Трейлеры, заглушка, мерзкая хуйня короче
        '75x75!',  // Паззлы, мелкие превьюхи
        '95x63!',  // Детская киноафиша, кадры
        'x65', // Постеры, информер, детская киноафиша
        '105x60!',  // Трейлеры (похожие)
        '290', // Постеры большие
        '220', // Новостная штука
        '290x190', // фотографии ТЦ
        '290x160!', // Постер киноклуба
        '1270', // Ширина трейлеровского кадра
        '900x600', // Кадр на мобильной версии
        '210x120', // Превью для трейлера
        '930x450', // Обложка кинотеатра
        '600x285', // Обложка фильма для рассылки после сеанса
        '260', // Постер на главной на мобиле
        '95x70', // Пресспоказы превьюха
        '150x110', // Пресспоказы превьюха
        '320', // Кадр для нового вида на мобиле
        '260x150', // Картинка подборки
        '190x130', // Картинка для новости в карточке фильма
        '450x260', // Подборки онлайн

        '1080x1920', // Постеры для приложения
        '310x440', // Постер превью для приложения
        '316x210', // Кадр пресью для приложения
        '120х180',
        '292х440',
        '1080х1920',
        '310x440',
        '45x45', // Лого фестиваля
        '35x35', // Лого фестиваля мобайл

        '180x180', // APIv2 news
        '520x364', // APIv2 news
        '60x34', // Mobile articles main page
        '120x120', // Interview main page
        '60x60', // Interview main page
        '930x315', // Widget votes
        '290x355',

        '955x500', // Шаринг фб
        '1074x480', // Шаринг вк
        '30x40', // Постеры, фото в новостях
        '776', // Новая ширина картинок в новостях
        'x160', // В списках на новой странице новостей картинка
        'x40', // Размер картинки в превью галереи
        '780x525', // Размер картинки в новости в галерею

        '640x360', // Картинка для новостей мобайл
        'x300', // Катинка для новостей мобайл
        '500x350', // Катинка для новостей мобайл

        '930x500', // Карусель
        '290x160', // Топовые новости в списках новостей
        '610', // Обычные новости в списке новостей
        '250x175', // Мобильные новости
        '1300x684', // Шаринг для facebook, vk

        '930x530', // Allcafe review

        1000,
        190,
        56,
        70,
        60,
        209, // Кадры pda
        75, // Быстрый поиск (фильмы)
        67, // Footer wallpapers
        76, // Footer puzzles
        580, // Новости (большая картинка)
        90, // Новости (маленькая картинка на главной странице)
        46,
        275, // Постеры фильмов для экспорта жидам или как там их
        94, // Постеры фильмов в детской киноафише
        161, // Картинки в карточке фильма
        110, // Постеры в слайдере для мобильной
        60, // Постер фильма для мобильной в списках фильмов
        210,// Постер на большом сайте новый
        261, // Сайт кинотеатра постер
        65, // Постер для шапки на десктопе
        145, // Фильмы (похожие фильмы)
        130, // Фильмы (детская киноафиша)
        137, // Фильмы (детская киноафиша)
        63, // Кадры pda
    ];

    /**
     * Качество сохраняемых превью
     */
    const QUALITY = 90;

    /**
     * Цвет текста поверх превью
     */
    const PREVIEW_TITLE_COLOR = '#fff';

    /**
     * Количество символов на строку, если надо будет разбить длинную строку на несколько
     */
    const MAX_LINE_LENGTH = 32;

    /**
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * Параметры рисования
     *
     * @var CanvasConfiguration
     */
    protected $config;

    /**
     * Куда сохранять итоговую картинку
     *
     * @var string
     */
    protected $destPath;

    /**
     * Желаемый размер (200x200, 580)
     *
     * @var int|string
     */
    protected $size;

    /**
     * Строки для написания
     *
     * @var array
     */
    protected $titleLines;

    /**
     * Можно ли вырезать отдельные части. Допустим если картинка 200x200, а нам нужно
     * 100x50, мы ресайзним исходную картинку до 100x100, и потом возьмем 100x50 из центра
     *
     * @var bool
     */
    protected $canvas;

    /**
     * Canvas constructor.
     *
     * @param CanvasConfiguration $config
     * @param string              $destPath
     * @param int|string          $size
     * @param string              $title
     * @param bool                $canvas
     */
    public function __construct(CanvasConfiguration $config, $destPath, $size, $title, $canvas = false)
    {
        $this->config     = $config;
        $this->destPath   = $destPath;
        $this->size       = $size;
        $this->titleLines = $this->splitMultilineText(mb_wordwrap($title, ($this->config->getTextArea()!==false ? $this->config->getTextArea()['maxLineLength'] : static::MAX_LINE_LENGTH)));
        $this->canvas     = $canvas;

        $this->imageManager = new ImageManager(['driver' => $config->getImageDriver()]);
    }

    /**
     * Пытается привести размер картинки к желаемому
     *
     * @param Image      $image
     * @param string|int $size   Желаемый размер (200x200, 580)
     * @param bool       $canvas Можно ли вырезать отдельные части. {@see resize()}
     */
    protected function resizeImage(Image $image, $size, $canvas = false)
    {
        [$width, $height] = $this->parseImageSize($size);

        if ($canvas) {
            if ($height !== null) {
                $aspectRatio = $image->width() / $image->height();

                if ($image->height() - ($image->width() - $width) / $aspectRatio > $height) {
                    $image->widen($width);
                } else {
                    $image->heighten($height);
                }
            }

            $image->resizeCanvas($width, $height);
        } else {
            $image->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
    }

    /**
     * Возвращает желаемую ширину и высоту
     *
     * @param string|int $size Желаемый размер (200x200, 580)
     * @return array
     */
    protected function parseImageSize($size)
    {
        if (is_int($size)) {
            return [$size, null];
        }

        return explode('x', $size);
    }

    /**
     * Добавляет текст на картинку
     * Текст может быть многострочный
     * Старается сделать размер текста относительным
     *
     * @param Image $image
     * @param array $lines  Строки для написания
     * @param float $fontSize
     * @param float $lineHeight
     * @param float $startY Откуда начинаем писать. Пишем мы кстати снизу вверх, чтобы текст был прибит к низу
     */
    protected function addMultilineTextToImage(Image $image, $lines, $fontSize, $lineHeight, $startY)
    {
        // Нам нужно делать отсутп снизу, поэтому будем идти по строкам в обратном порядке
        for ($i = count($lines) - 1, $y = $startY; $i >= 0; $i--, $y -= $lineHeight) {
            $image->text($lines[$i], $image->width() / 2, $y, function ($font) use ($fontSize) {
                $font->file($this->config->getFontPath());
                $font->size($fontSize);
                $font->color(static::PREVIEW_TITLE_COLOR);
                $font->align('center');
            });
        }
    }

    /**
     * Разбивает строку на несколько если в строке есть символ перевода на новую строку
     * или строка слишком длинная
     *
     * @param string $text
     * @return array
     */
    protected function splitMultilineText($text)
    {
        if (strpos($text, PHP_EOL) === false) {
            $text = mb_wordwrap($text, static::MAX_LINE_LENGTH);
        }

        return array_map('trim', explode(PHP_EOL, $text));
    }
}
