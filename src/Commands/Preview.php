<?php

namespace Kinoafisha\Picasso\Commands;

use Intervention\Image\Image;

abstract class Preview extends Canvas
{
    /**
     * Путь к картинке для написания текста на ней
     *
     * @var string
     */
    protected $sourcePath;

    /**
     * Preview constructor.
     *
     * @param string $sourcePath
     * @param array  $params
     */
    public function __construct($sourcePath, ...$params)
    {
        parent::__construct(...$params);

        $this->sourcePath = $sourcePath;
    }

    /**
     * Генерирует фон для превью: если картинка существует, то берем ее, иначе дефолтный фон,
     * затем накладываем подложку в зависимости от того, делаем ли мы превью для трейлера
     *
     * @param string     $sourcePath Путь до картинки
     * @param string|int $size       Желаемый размер (200x200, 580)
     * @param bool       $canvas     Можно ли вырезать отдельные части. {@see resize()}
     * @return Image
     */
    protected function makePreviewBackground($sourcePath, $size, $canvas)
    {
        $image = $this->imageManager->make($sourcePath);

        $this->resizeImage($image, $size, $canvas);

        // Вставляем "watermark"
        $background = $this->imageManager->make($this->config->getBackgroundPath());

        $background->resize($image->width(), null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->insert($background, 'bottom');

        return $image;
    }
}
