<?php

namespace Kinoafisha\Picasso\Commands;

use Kinoafisha\Picasso\Contracts\Command;

class MakePreview extends Preview implements Command
{
	/**
	 * Делает превью для статьи
	 */
	public function execute()
	{
		$image = $this->makePreviewBackground($this->sourcePath, $this->size, $this->canvas);
		
		if($this->config->getTextArea()==false){
			$fontSize   = intval($image->width() * 54 / 1300); // Линейная зависимость
			$lineHeight = intval($fontSize * 60 / 54); // Линейная зависимость
			
			$startY = intval($image->height() - ($image->height() * 130 / 684)); // Линейная зависимость
		} else {
			$fontSize   = intval($image->width() * $this->config->getTextArea()['fontSize'] / 1300); // Линейная зависимость
			$lineHeight = intval($fontSize * 60 / $this->config->getTextArea()['fontSize']); // Линейная зависимость
			
			$boxHeight = $image->height()-$this->config->getTextArea()['marginTop'];
			$startY = intval($boxHeight/2)+intval($lineHeight*count($this->titleLines)/2) + $this->config->getTextArea()['marginTop']/2;
		}
		$this->addMultilineTextToImage(
			$image,
			$this->titleLines,
			$fontSize,
			$lineHeight,
			$startY
		);
		
		$image->save($this->destPath, static::QUALITY);
	}
}
