<?php

namespace Kinoafisha\Picasso\Commands;

use Kinoafisha\Picasso\Contracts\Command;
use function Kinoafisha\Picasso\mb_wordwrap;

class MakeTrailerPreview extends Preview implements Command
{
    /**
     * Название фильма, разбитое по строкам
     *
     * @var array
     */
    protected $movieNameLines;

    /**
     * MakeTrailerPreview constructor.
     *
     * @param string $movieName
     * @param array  $params
     */
    public function __construct($movieName, ...$params)
    {
        parent::__construct(...$params);

        $this->movieNameLines = $this->splitMultilineText(mb_wordwrap($movieName, static::MAX_LINE_LENGTH));
    }

    /**
     * Делает превью для трейлера
     */
    public function execute()
    {
        $image = $this->makePreviewBackground($this->sourcePath, $this->size, $this->canvas);

        $movieNameFontSize   = intval($image->width() * 54 / 1300); // Линейная зависимость
        $movieNameLineHeight = intval($movieNameFontSize * 60 / 54); // Линейная зависимость

        $this->addMultilineTextToImage(
            $image,
            $this->movieNameLines,
            $movieNameFontSize,
            $movieNameLineHeight,
            intval($image->height() - ($image->height() * 200 / 684)) // Линейная зависимость
        );

        $titleFontSize   = intval($image->width() * 36 / 1300); // Линейная зависимость
        $titleLineHeight = intval($titleFontSize * 42 / 36); // Линейная зависимость

        $this->addMultilineTextToImage(
            $image,
            $this->titleLines,
            $titleFontSize,
            $titleLineHeight,
            intval($image->height() - ($image->height() * 140 / 684)) // Линейная зависимость
        );

        $image->save($this->destPath, static::QUALITY);
    }
}
