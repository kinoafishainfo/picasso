<?php

namespace Kinoafisha\Picasso\Commands;

use Kinoafisha\Picasso\Contracts\Command;

class MakeAnnounce extends Announce implements Command
{
	/**
	 * Делает превью для статьи без изображения
	 */
	public function execute()
	{
		$image = $this->makeAnnounceBackground($this->size, $this->canvas);
		
		if($this->config->getTextArea()==false){
			$fontSize   = intval($image->width() * 54 / 1300); // Линейная зависимость
			$lineHeight = intval($fontSize * 60 / 54); // Линейная зависимость
			// Тут мы найдем середину высоты, потом прибавим половину высоты текста.
			// Высота текста считается так: lineHeight показывает отступ между строками, поэтому мы
			// посчитаем его n - 1 раз (n - количество строк), потому что первая строка не имеет отступа
			// (сверху ничего нет), затем прибавим 0.6 * fontSize потому что fontSize - это не высота "среднего"
			// текста, а высота строчной буквы, поэтому мы домножим на 0.6 (в среднем в тексте больше строчных букв)
			$startY = intval($image->height() / 2 + ($fontSize * 0.6 + (count($this->titleLines) - 1) * $lineHeight) / 2);
		} else {
			$fontSize   = intval($image->width() * $this->config->getTextArea()['fontSize'] / 1300); // Линейная зависимость
			$lineHeight = intval($fontSize * 60 / $this->config->getTextArea()['fontSize']); // Линейная зависимость
			
			$boxHeight = $image->height()-$this->config->getTextArea()['marginTop'];
			$startY = intval($boxHeight/2)+intval($lineHeight*count($this->titleLines)/2) + $this->config->getTextArea()['marginTop']/2;
		}
		
		$this->addMultilineTextToImage(
				$image,
				$this->titleLines,
				$fontSize,
				$lineHeight,
				$startY
				);
		
		$image->save($this->destPath, static::QUALITY);
	}
}
