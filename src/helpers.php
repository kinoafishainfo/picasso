<?php

namespace Kinoafisha\Picasso;

/**
 * Wraps a multibyte string to a given number of characters
 *
 * @link https://php.net/manual/en/function.wordwrap.php
 * @see  https://stackoverflow.com/questions/3825226/multi-byte-safe-wordwrap-function-for-utf-8/4988494#4988494
 * @param string $str   The input string.
 * @param int    $width The column width.
 * @param string $break The line is broken using the optional break parameter.
 * @param bool   $cut   If the cut is set to true, the string is
 *                      always wrapped at or before the specified width. So if you have
 *                      a word that is larger than the given width, it is broken apart.
 * @return string string the given string wrapped at the specified column.
 */
function mb_wordwrap($str, $width = 75, $break = "\n", $cut = false)
{
    $lines = explode($break, $str);

    foreach ($lines as &$line) {
        $line = rtrim($line);

        if (mb_strlen($line) <= $width) {
            continue;
        }

        $words = explode(' ', $line);
        $line = '';
        $actual = '';

        foreach ($words as $word) {
            if (mb_strlen($actual . $word) <= $width) {
                $actual .= $word . ' ';
            } else {
                if ($actual != '') {
                    $line .= rtrim($actual) . $break;
                }

                $actual = $word;

                if ($cut) {
                    while (mb_strlen($actual) > $width) {
                        $line .= mb_substr($actual, 0, $width) . $break;
                        $actual = mb_substr($actual, $width);
                    }
                }

                $actual .= ' ';
            }
        }

        $line .= trim($actual);
    }

    return implode($break, $lines);
}
