<?php

namespace Kinoafisha\Picasso\Contracts;

interface Command
{
    /**
     * Выполняет что-то
     */
    public function execute();
}
