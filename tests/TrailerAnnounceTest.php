<?php

namespace Tests;

use Kinoafisha\Picasso\Commands\MakeTrailerAnnounce;

class TrailerAnnounceTest extends TrailerTest
{
    /**
     * @dataProvider provider
     */
    public function testTrailerPreview($canvas, $filename)
    {
        $makeTrailerAnnounceCommand = new MakeTrailerAnnounce(
            '"Мстители: Финал"',
            $this->config,
            $this->destPath,
            '1300x684',
            'Первый трейлер',
            $canvas
        );

        $this->invoker->setCommand($makeTrailerAnnounceCommand);

        $this->invoker->run();

        $this->assertImagesEquals(__DIR__ . "/assets/{$filename}", $this->destPath);
    }

    public function provider()
    {
        return [
            [false, 'trailer_announce.jpg'],
            [true, 'canvas_trailer_announce.jpg'],
        ];
    }
}
