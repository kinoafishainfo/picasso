<?php

namespace Tests;

use Kinoafisha\Picasso\Commands\MakeTrailerPreview;

class TrailerPreviewTest extends CanvasTest
{
    /**
     * @dataProvider provider
     */
    public function testTrailerPreview($canvas, $filename)
    {
        $makeTrailerPreviewCommand = new MakeTrailerPreview(
            '"Мстители: Финал"',
            __DIR__ . '/assets/jw3.jpeg',
            $this->config,
            $this->destPath,
            '1300x684',
            'Первый трейлер',
            $canvas
        );

        $this->invoker->setCommand($makeTrailerPreviewCommand);

        $this->invoker->run();

        $this->assertImagesEquals(__DIR__ . "/assets/{$filename}", $this->destPath);
    }

    public function provider()
    {
        return [
            [false, 'trailer_preview.jpeg'],
            [true, 'canvas_trailer_preview.jpeg'],
        ];
    }
}
