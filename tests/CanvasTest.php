<?php

namespace Tests;

use Kinoafisha\Picasso\CanvasConfiguration;
use Kinoafisha\Picasso\Invoker;

abstract class CanvasTest extends TestCase
{
    /**
     * @var Invoker
     */
    protected $invoker;

    /**
     * @var CanvasConfiguration
     */
    protected $config;

    /**
     * @var string
     */
    protected $destPath;

    protected function setUp()
    {
        $this->invoker = new Invoker;

        $this->config = new CanvasConfiguration(
            __DIR__ . '/assets/bg/preview-background.png',
            __DIR__ . '/assets/bg/empty-preview-background.png',
            __DIR__ . '/assets/fonts/bold.ttf'
        );

        $this->destPath = tempnam(sys_get_temp_dir(), 'canvas');
    }
}
