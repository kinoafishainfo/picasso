<?php

namespace Tests;

use Kinoafisha\Picasso\Commands\MakePreview;

class PreviewTest extends CanvasTest
{
    /**
     * @dataProvider provider
     */
    public function testPreview($canvas, $filename)
    {
        $makePreviewCommand = new MakePreview(
            __DIR__ . '/assets/eng.jpg',
            $this->config,
            $this->destPath,
            '1300x684',
            "Главные сериалы осени: «Маньяк»,\n«Звоните ДиКаприо!» и «Сарбина»\nприключения",
            $canvas
        );

        $this->invoker->setCommand($makePreviewCommand);

        $this->invoker->run();

        $this->assertImagesEquals(__DIR__ . "/assets/{$filename}", $this->destPath);
    }

    public function provider()
    {
        return [
            [false, 'preview.jpg'],
            [true, 'canvas_preview.jpg'],
        ];
    }
}
