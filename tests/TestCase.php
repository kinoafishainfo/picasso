<?php

namespace Tests;

use Tests\Constraints\IsImagesEquals;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * Asserts that the contents of one image is equal to the contents of another image
     *
     * @param string $expected
     * @param string $actual
     * @param string $message
     */
    public static function assertImagesEquals(string $expected, string $actual, string $message = ''): void
    {
        static::assertFileExists($expected, $message);
        static::assertFileExists($actual, $message);

        static::assertThat(
            $actual,
            new IsImagesEquals($expected),
            $message
        );
    }
}
