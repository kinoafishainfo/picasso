<?php

namespace Tests;

use Kinoafisha\Picasso\Commands\MakeAnnounce;

class AnnounceTest extends CanvasTest
{
    /**
     * @dataProvider provider
     */
    public function testAnnounce($canvas, $filename)
    {
        $makeAnnounceCommand = new MakeAnnounce(
            $this->config,
            $this->destPath,
            '1300x684',
            "Главные сериалы осени: «Маньяк»,\n«Звоните ДиКаприо!» и «Сарбина»\nприключения",
            $canvas
        );

        $this->invoker->setCommand($makeAnnounceCommand);

        $this->invoker->run();

        $this->assertImagesEquals(__DIR__ . "/assets/{$filename}", $this->destPath);
    }

    public function provider()
    {
        return [
            [false, 'announce.jpg'],
            [true, 'canvas_announce.jpg'],
        ];
    }
}
