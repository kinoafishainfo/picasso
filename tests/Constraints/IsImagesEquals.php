<?php

namespace Tests\Constraints;

use Imagick;
use PHPUnit\Framework\Constraint\Constraint;

class IsImagesEquals extends Constraint
{
    /**
     * @var Imagick
     */
    protected $expected;

    /**
     * IsImagesEquals constructor.
     *
     * @param string $expected
     */
    public function __construct($expected)
    {
        parent::__construct();

        $this->expected = $expected;
    }


    /**
     * Evaluates the constraint for parameter $other. Returns true if the
     * constraint is met, false otherwise.
     *
     * This method can be overridden to implement the evaluation algorithm.
     *
     * @param mixed $other value or object to evaluate
     * @return bool
     */
    protected function matches($other): bool
    {
        try {
            $expected = new Imagick($this->expected);

            $other = new Imagick($other);

            $result = $other->compareImages($expected, Imagick::METRIC_MEANSQUAREERROR);

            return $result[1] === 0.0;
        } catch (\ImagickException $e) {
            return false;
        }
    }

    /**
     * Returns a string representation of the object.
     */
    public function toString(): string
    {
        return 'картинки равны';
    }
}
