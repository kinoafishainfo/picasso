<?php

namespace Tests;

use Kinoafisha\Picasso\Commands\Canvas;
use function Kinoafisha\Picasso\mb_wordwrap;

class HelpersTest extends TestCase
{
    public function testMbWordWrap()
    {
        $this->assertEquals(
            "Главные сериалы осени: «Маньяк», «Звоните ДиКаприо!» и «Сарбина»\nприключения",
            mb_wordwrap('Главные сериалы осени: «Маньяк», «Звоните ДиКаприо!» и «Сарбина» приключения')
        );

        $this->assertEquals(
            "Главные сериалы осени: «Маньяк»,\n«Звоните ДиКаприо!» и «Сарбина»\nприключения",
            mb_wordwrap(
                'Главные сериалы осени: «Маньяк», «Звоните ДиКаприо!» и «Сарбина» приключения',
                Canvas::MAX_LINE_LENGTH
            )
        );

        $this->assertEquals("Woo\noho", mb_wordwrap('Woooho', 3, PHP_EOL, true));
    }
}
