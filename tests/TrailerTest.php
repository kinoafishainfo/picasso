<?php

namespace Tests;

use Kinoafisha\Picasso\CanvasConfiguration;

abstract class TrailerTest extends CanvasTest
{
    protected function setUp()
    {
        parent::setUp();

        $this->config = new CanvasConfiguration(
            __DIR__ . '/assets/bg/trailer-preview-background.png',
            __DIR__ . '/assets/bg/empty-preview-background.png',
            __DIR__ . '/assets/fonts/bold.ttf'
        );
    }
}
