# Picasso

Этот пакет нужен для нанесения заголовков на изображения для превью статей

Пожалуйста, перед пушем делайте 
```bash
$ composer test && composer check-style 
```
И постарайтесь держать code coverage > 90%

## Install

Добавьте в свой composer.json

``` json
{
    "repositories": [
        {
            "type": "git",
            "url": "git@bitbucket.org:kinoafishainfo/picasso.git"
        }
    ],
    "require": {
        "kinoafishainfo/picasso": "dev-master"
    }
}
```

## Usage

``` php
<?php

$invoker = new Invoker;

$config = new CanvasConfiguration(
    dirname(__DIR__) . '/assets/preview-background.png',
    dirname(__DIR__) . '/assets/empty-preview-background.png',
    __DIR__ . '/assets/fonts/bold.ttf'
);

$destPath = tempnam(sys_get_temp_dir(), 'canvas');

$makePreviewCommand = new MakePreview(
    __DIR__ . '/assets/eng.jpg',
    $config,
    $destPath,
    '1300x684',
    "Главные сериалы осени: «Маньяк»,\n«Звоните ДиКаприо!» и «Сарбина»\nприключения",
    $canvas
);

$invoker->setCommand($makePreviewCommand);

$invoker->run();
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.
