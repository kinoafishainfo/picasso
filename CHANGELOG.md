# Changelog

All notable changes to `picasso` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## 2019-02-19

### Init
